inapp.add( "echart_view", {
    cfg: {
        oneBarcharts: [],
        circleCharts: [],
        dounutCharts: [],
        pictorialCharts: [],
        barCharts: []
    },
    init: function () {
        this.create();
    },
    actionMap: {
        "echart_data_manger": {
            "chart_data": function ( data ) {

                this.load( data.data );

            }
        }
    },
    create: function () {
        charts = [];
        //차트 추가시 밑에 추가하고, 옵션에 맞는 함수의 밑에다 추가 설정을한다.
        charts.push( this.echart_init( "oneBar", $( '#yearsServiceChart' )[ 0 ] ) );
        charts.push( this.echart_init( "oneBar", $( '#salaryIncreaseChart' )[ 0 ] ) );
        charts.push( this.echart_init( "oneBar", $( '#incentiveChart' )[ 0 ] ) );
        charts.push( this.echart_init( "dounut", $( '#budgetChart' )[ 0 ] ) );
        charts.push( this.echart_init( "pictorial", $( '#personnelInfoChart' )[ 0 ] ) );
        charts.push( this.echart_init( "circle", $( '#evaluationChart' )[ 0 ] ) );
        charts.push( this.echart_init( "circle", $( '#genderChart' )[ 0 ] ) );
        charts.push( this.echart_init( "pictorial", $( '#ageArgChart' )[ 0 ] ) );
        charts.push( this.echart_init( "circle", $( '#laborCostsChart' )[ 0 ] ) );
        charts.push( this.echart_init( "circle", $( '#typeChart' )[ 0 ] ) );
        charts.push( this.echart_init( "circle", $( '#dutyChart' )[ 0 ] ) );
        charts.push( this.echart_init( "circle", $( '#occupationsChart' )[ 0 ] ) );
        charts.push( this.echart_init( "bar", $( '#workingTimeChart' )[ 0 ] ) );
        charts.push( this.echart_init( "bar", $( '#vacationDaysChart' )[ 0 ] ) );
        charts.push( this.echart_init( "bar", $( '#timeWorkChart' )[ 0 ] ) );
        charts.push( this.echart_init( "bar", $( '#joinLeaveChart' )[ 0 ] ) );
        //차트 추가시 여기에

        this.resize( charts );
    },
    echart_init: function ( type, value ) {
        var chartOptions, setting;

        chartOptions = this.basicChartOption();
        setting = echarts.init( value );

        if ( type == 'oneBar' ) {
            setting.setOption( chartOptions.oneBarChartOption );
            this.cfg.oneBarcharts.push( setting );

        } else if ( type == 'circle' ) {
            setting.setOption( chartOptions.circleChartOption );
            this.cfg.circleCharts.push( setting );

        } else if ( type == 'dounut' ) {
            setting.setOption( chartOptions.dounutChartOption );
            this.cfg.dounutCharts.push( setting );

        } else if ( type == 'pictorial' ) {
            setting.setOption( chartOptions.pictorialChartOption );
            this.cfg.pictorialCharts.push( setting );

        } else if ( type == 'bar' ) {
            setting.setOption( chartOptions.barChartOption );
            this.cfg.barCharts.push( setting );
        }

        return setting;
    },
    resize: function ( charts ) {
        var chart;

        chart = charts;

        $( window ).resize( function () {
            //제이커리 리사이즈 이벤트
            var i, length;

            for ( i = 0, length = chart.length; i < length; i++ ) {
                charts[ i ].resize();
            }

        } );
    },
    load: function ( trueData ) {
        // 데이터 로드에 성공하면 자동으로 순서대로 해당 타입에 맞게 데이터를 가져와 그래프에 set하는 함수를 호출해줌 손댈필요x
        var data, oneBarCharts, circleCharts, dounutCharts, pictorialCharts, barCharts, length, i;

        data = trueData;
        oneBarcharts = this.cfg.oneBarcharts;
        circleCharts = this.cfg.circleCharts;
        dounutCharts = this.cfg.dounutCharts;
        pictorialCharts = this.cfg.pictorialCharts;
        barCharts = this.cfg.barCharts;

        for ( i = 0, length = oneBarcharts.length; i < length; i++ ) {
            oneBarcharts[ i ].setOption( this.oneBarChartOption( i, data ) );
        }
        for ( i = 0, length = circleCharts.length; i < length; i++ ) {
            circleCharts[ i ].setOption( this.circleChartOption( i, data ) );
        }
        for ( i = 0, length = dounutCharts.length; i < length; i++ ) {
            dounutCharts[ i ].setOption( this.dounutChartOption( i, data ) );
        }
        for ( i = 0, length = pictorialCharts.length; i < length; i++ ) {
            pictorialCharts[ i ].setOption( this.pictorialChartOption( i, data ) );
        }
        for ( i = 0, length = barCharts.length; i < length; i++ ) {
            barCharts[ i ].setOption( this.barChartOption( i, data ) );
        }

    },
    //개별 막대형 차트 옵션 개별 설정하기(차트 순서대로 할것)
    oneBarChartOption: function ( params, trueData ) {

        var option = [];

        option = [ {
                xAxis: [ {
                    max: 50,
                    min: 0
                } ],
                series: [ {
                    markLine: {
                        data: [ {
                            xAxis: 35
                        } ]
                    },
                    data: trueData.continuousYear
                } ]
            },

            {
                series: [ {
                    markLine: {
                        data: [ {
                            xAxis: 85
                        } ]
                    },
                    data: trueData.salaryIncrease
                } ]
            },

            {
                series: [ {
                    markLine: {
                        data: [ {
                            xAxis: 72
                        } ]
                    },
                    data: trueData.incentiveArg
                } ]
            }
        ];

        return option[ params ];
    },
    //개별 원형 차트 옵션 개별 설정하기(차트 순서대로 할것)
    circleChartOption: function ( params, trueData ) {
        var option = [];

        option = [ {
                legend: {
                    data: [ 'A', 'B', 'C', 'D', 'E' ]
                },
                series: [ {
                    name: 'evaluation',
                    data: trueData.evaluation
                } ]
            },

            {
                legend: {
                    data: [ '남', '여' ]
                },
                series: [ {
                    name: 'gender',
                    data: trueData.gender
                } ]
            },

            {
                legend: {
                    data: [ trueData.laborCosts[ 0 ].name, trueData.laborCosts[ 1 ].name ]
                },
                series: [ {
                    name: 'laborCosts',
                    data: trueData.laborCosts,
                    label: {
                        formatter: '{d}% \n ({c} 만원)'
                    }
                } ]
            },

            {
                legend: {
                    data: [ 'A', 'B', 'C', 'D', 'E' ]
                },
                series: [ {
                    name: 'type',
                    data: trueData.type
                } ]
            },

            {
                legend: {
                    data: [ '기술', '기획', '디자인', '영업', '스탭' ]
                },
                series: [ {
                    name: 'duty',
                    data: trueData.duty
                } ]
            },

            {
                legend: {
                    data: [ '경영', '영업', '생산', '전문', '서비스' ]
                },
                series: [ {
                    name: 'occupations',
                    data: trueData.occupations
                } ]
            }
        ];

        return option[ params ];
    },
    //개별 도넛형 차트 옵션 개별 설정하기(차트 순서대로 할것)
    dounutChartOption: function ( params, trueData ) {
        var option = [];

        option = [ {
            legend: {
                data: [ '집행 예산', '가용 예산' ]
            },
            series: [ {
                    name: 'budget',
                    data: trueData.budget
                },
                {
                    label: {
                        formatter: '{b} \n {c}만원'
                    },
                    data: [ {
                        name: '총 예산',
                        value: 10000
                    } ]
                }
            ]
        } ];

        return option[ params ];
    },
    //개별 픽토리얼 차트 옵션 개별 설정하기(차트 순서대로 할것)
    pictorialChartOption: function ( params, trueData ) {

        var option = [];

        option = [ {
                series: [ {
                        name: 'man',
                        symbolBoundingData: 10,
                        label: {
                            formatter: '남 {c} 명'
                        },
                        data: [ {
                            value: trueData.gender[ 0 ].value,
                            symbol: trueData.symbol.oneman
                        } ]
                    },
                    {
                        name: '-',
                        symbolBoundingData: 10,
                        label: {
                            formatter: '1 : {c} '
                        },
                        data: [ {
                            value: trueData.personnelInfo.value.toFixed( 1 ),
                            symbol: "none"
                        } ]
                    },
                    {
                        name: 'woman',
                        symbolBoundingData: 10,
                        label: {
                            formatter: '여 {c} 명'
                        },
                        data: [ {
                            value: trueData.gender[ 1 ].value,
                            symbol: trueData.symbol.manyman
                        } ]
                    }
                ]
            },

            {
                series: [ {
                        name: 'man',
                        symbolBoundingData: 55,
                        label: {
                            formatter: '{c} 세',
                        },
                        data: [ {
                            value: trueData.age[ 0 ].value.toFixed( 1 ),
                            symbol: trueData.symbol.man
                        } ]
                    },
                    {
                        name: '-',
                        symbolBoundingData: 55,
                        data: []
                    },
                    {
                        name: 'woman',
                        symbolBoundingData: 55,
                        label: {
                            formatter: '{c} 세',
                        },
                        data: [ {
                            value: trueData.age[ 1 ].value.toFixed( 1 ),
                            symbol: trueData.symbol.woman
                        } ]
                    }
                ]
            }

        ];

        return option[ params ];
    },
    //개별 바(기본형) 차트 옵션 개별 설정하기(차트 순서대로 할것)
    barChartOption: function ( params, trueData ) {
        var option = [];

        option = [ {
                legend: {
                    data: [ '소정', '시간외' ]
                },
                xAxis: [ {
                    data: [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월' ]
                } ],
                yAxis: [ {
                    name: '평균근로시간(H)',
                    max: 15,
                    min: 0
                } ],
                series: [ {
                        name: '소정',
                        type: 'bar',
                        stack: 'one',
                        label: {
                            show: true,
                            formatter: '{c}',
                        },
                        data: trueData.working_hours
                    },
                    {
                        name: '시간외',
                        type: 'bar',
                        stack: 'one',
                        label: {
                            show: true,
                            formatter: '{c}',
                        },
                        data: trueData.out_working_hours
                    }
                ]
            },

            {
                legend: {
                    data: [ '휴가사용일' ]
                },
                xAxis: [ {
                    data: [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월' ]
                } ],
                yAxis: [ {
                    name: '평균휴가사용일(일)',
                    max: 1000,
                    min: 0
                } ],
                series: [ {
                    name: '휴가사용일',
                    type: 'bar',
                    stack: 'one',
                    label: {
                        show: true,
                        position: 'top',
                        formatter: '{c}',
                    },
                    data: trueData.vacation_days
                } ]
            },

            {
                legend: {
                    data: [ '출퇴근시간' ],
                },
                xAxis: [ {
                    data: [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월' ]
                } ],
                yAxis: [ {
                    name: '평균 출퇴근 시간(H)',
                    axisLabel: {
                        show: true,
                        formatter: function ( params ) {
                            var value, hours, minutes;
                            value = new Date( params );
                            hours = value.getHours();
                            minutes = value.getMinutes();
                            if ( value.getMinutes().toString().length < 2 ) {
                                minutes = "0" + value.getMinutes();
                            } else {
                                minutes = value.getMinutes();
                            }

                            return hours + ":" + minutes;
                        },
                    },
                    max: 1427295600000,
                    min: 1427209200000

                } ],
                series: [ {
                        name: 'name',
                        type: 'bar',
                        stack: 'one',
                        label: {
                            show: true,
                            position: 'top',
                            textStyle: {
                                color: '#3398DB'
                            },
                            offset: [ 0, 20 ],
                            formatter: function ( params ) {
                                var value, hours, minutes;
                                value = new Date( params.value );
                                hours = value.getHours();
                                minutes = value.getMinutes();
                                if ( value.getMinutes().toString().length < 2 ) {
                                    minutes = "0" + value.getMinutes();
                                } else {
                                    minutes = value.getMinutes();
                                }

                                return hours + ":" + minutes;
                            },
                        },
                        itemStyle: {
                            barBorderColor: 'rgba(0,0,0,0)',
                            color: 'rgba(0,0,0,0)'
                        },
                        emphasis: {
                            itemStyle: {
                                barBorderColor: 'rgba(0,0,0,0)',
                                color: 'rgba(0,0,0,0)'
                            }
                        },
                        data: trueData.time_on_work
                    },
                    {
                        name: '출퇴근시간',
                        type: 'bar',
                        stack: 'one',
                        label: {
                            show: true,
                            position: 'top',
                            formatter: function ( params ) {
                                var value, hours, minutes;
                                value = new Date( params.value );
                                hours = value.getHours();
                                minutes = value.getMinutes();
                                if ( value.getMinutes().toString().length < 2 ) {
                                    minutes = "0" + value.getMinutes();
                                } else {
                                    minutes = value.getMinutes();
                                }

                                return hours + ":" + minutes;
                            },
                        },
                        data: trueData.time_off_work
                    }
                ]
            },

            {
                legend: {
                    data: [ '입사자', '퇴사자' ]
                },
                xAxis: [ {
                    data: [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월' ]
                } ],
                yAxis: [ {
                    name: '인원(명)',
                    max: 1500,
                    min: 0
                } ],
                series: [ {
                        name: '입사자',
                        type: 'bar',
                        barGap: 1,
                        label: {
                            show: true,
                            position: 'top',
                            formatter: '{c}',
                        },
                        data: trueData.join_number
                    },
                    {
                        name: '퇴사자',
                        type: 'bar',
                        label: {
                            show: true,
                            position: 'top',
                            formatter: '{c}',
                        },
                        data: trueData.leave_number
                    }
                ]
            }

        ];

        return option[ params ];
    },
    //차트 기본옵션
    basicChartOption: function () {
        var option;

        option = {
            oneBarChartOption: {
                grid: {
                    top: 'center',
                    left: '3%',
                    right: '4%',
                    bottom: '100%',
                    containLabel: true
                },
                xAxis: [ {
                        type: 'value',
                        splitLine: {
                            show: false
                        },
                        boundaryGap: [ 0, 0.1 ],
                        max: 100,
                        min: 0
                    },
                    {
                        type: 'value'
                    }
                ],
                yAxis: {
                    type: 'category',
                    splitArea: {
                        show: true
                    },
                    data: [ '' ]
                },
                series: [ {
                    name: 'incentive',
                    type: 'bar',
                    color: '#003366',
                    markLine: {
                        label: {
                            show: false
                        },
                        symbol: [ 'none', 'none' ],
                        lineStyle: {
                            color: '#d48265',
                            type: 'solid'
                        },
                        data: [ {
                            xAxis: 35
                        } ]
                    },
                    data: []
                } ]
            },
            circleChartOption: {
                legend: {
                    bottom: 10,
                    left: 'center',
                    data: []
                },
                series: [ {
                    name: 'evaluation',
                    type: 'pie',
                    radius: '60%',
                    center: [ '50%', '50%' ],
                    data: [],
                    label: {
                        show: true,
                        position: 'top',
                        distance: 40,
                        formatter: '{d}% \n ({c})'
                    }
                } ]
            },
            dounutChartOption: {
                color: [ '#003366', '#ccc' ],
                legend: {
                    left: 'center',
                    bottom: 10,
                    data: []
                },
                series: [ {
                        type: 'pie',
                        radius: [ '40%', '60%' ],
                        avoidLabelOverlap: false,
                        label: {
                            show: true, //라벨 속성 사용여부
                            position: 'top',
                            distance: 40, // siries 와의
                            formatter: '{d}% \n ({c})'
                        },
                        data: []
                    },
                    {
                        type: 'pie',
                        radius: [ 0, '30%' ],
                        itemStyle: {
                            barBorderColor: 'rgba(0,0,0,0)',
                            color: 'rgba(0,0,0,0)'
                        },
                        label: {
                            position: 'center',
                            distance: 10,
                            textStyle: {
                                color: 'black',
                                fontSize: '15',
                                fontWeight: 'bold'
                            }
                        },
                        labelLine: {
                            show: false
                        },
                        data: []
                    }
                ]
            },
            pictorialChartOption: {
                color: [ '#003366', '#d48265', '#d48265' ],
                xAxis: {
                    data: [ 'a' ],
                    axisTick: {
                        show: false
                    },
                    axisLine: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    }
                },
                yAxis: {
                    axisLine: {
                        show: false
                    },
                    axisLabel: {
                        show: false
                    },
                    axisTick: {
                        show: false
                    },
                    splitLine: {
                        show: false
                    }
                },
                grid: {
                    top: 'center'

                },
                series: [ {
                        name: 'man',
                        type: 'pictorialBar',
                        barGap: '50%',
                        label: {
                            show: true,
                            position: 'bottom',
                            offset: [ 0, 10 ],
                            formatter: '{c} 세',
                        },
                        data: []
                    },
                    {
                        name: '-',
                        type: 'pictorialBar',
                        barGap: '50%',
                        label: {
                            show: true,
                            position: 'center',
                            offset: [ 0, 80 ],
                            formatter: '1 : {c} ',
                            fontSize: 20,
                            color: 'red'
                        },
                        data: []
                    },
                    {
                        name: 'woman',
                        type: 'pictorialBar',
                        barGap: '50%',
                        label: {
                            show: true,
                            position: 'bottom',
                            offset: [ 0, 10 ],
                        },
                        data: []
                    }
                ]
            },
            barChartOption: {
                legend: {
                    data: [],
                    bottom: 'bottom',
                    z: 10
                },
                color: [ '#3398DB', '#ccc' ],
                grid: {
                    left: '3%',
                    right: '4%',
                    containLabel: true
                },
                xAxis: [ {
                    type: 'category',
                    data: [],
                    axisTick: {
                        alignWithLabel: true
                    }
                } ],
                yAxis: [ {
                    name: '',
                    type: 'value',
                    nameLocation: 'middle',
                    nameTextStyle: {
                        verticalAlign: 'top',
                        lineHeight: -35
                    },
                    max: 15,
                    min: 0
                } ],
                series: []
            }
        };

        return option;
    }

} );