inapp.add( "echart_data_manger", {
    init: function () {

        this.create();

    },
    create: function () {
        var self;

        self = this;

        $.get( './data/data.json' ).done( function ( data ) {

            var userData,yearData, trueData, i, length; //총데이터 , 길이등

            var continuousYear, oldSalaryArg, newSalaryArg, incentiveSum, evaluationText, genderSumM, genderSumW, newSalaryM, newSalaryW, ageM, ageW ;// 변수들


            userData = data.user_data;
            yearData = data.year_data;
            trueData = {};

            continuousYear = 0;
            oldSalaryArg = 0;
            newSalaryArg = 0;
            incentiveSum = 0;
            evaluationText = "";
            genderSumM = 0;
            genderSumW = 0;
            newSalaryM = 0;
            newSalaryW = 0;
            ageM = 0;
            ageW = 0;
            dutyText = "";
            occupationsText = "";


            length=userData.length;

            //근속년수 계산
            for (  i = 0; i < length; i++ ) {
                continuousYear += userData[ i ].continuous_year;
            }
            


            oldSalaryArg = oldSalaryArg + userData[ i ].oldSalary;
                newSalaryArg = newSalaryArg + userData[ i ].newSalary;

                if ( userData[ i ].incentive ) {
                    incentiveSum++;
                }
            // evaluationText = ABACA
            // eval =[];    eval =['A','A','A'];
            // eval.push(data);
            // eval.length = 3


            /*
            evaluationData = {};

            for (  i = 0; i < length; i++ ) {
                eval = userData[ i ].evaluation;

                A

                if(!evaluationData[eval]) {
                    evaluationData[eval] = 0;
                } else {
                    evaluationData[eval]++;
                }


                /*
                evaluationData = {
                    "A" :0,
                    "B":0,
                    "C":0
                }

            }
            */

            trueData = {
                symbol : data.symbol,
                continuousYear: [ continuousYear / length ],
                salaryIncrease: [ ( newSalaryArg - oldSalaryArg ) / oldSalaryArg * 100 ],
                incentiveArg: [ incentiveSum / length * 100 ]
            };

            inapp.raise( self.name, {
                action: "chart_data",
                data: trueData
            } );
        });
    }
});