<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="ko">

<head>
    <title>INORG Sample</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="./lib/bootstrap/css/bootstrap.min.css">

    <style>
        html,
        body {
            height: 100% !important;
        }
    </style>

    <!-- 외부 라이브러리 -->
    <script type="text/javascript" src="./lib/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/bootstrap/js/bootstrap.min.js"></script>
    
    <script type="text/javascript" src="./lib/inapp.js"></script>

    <!-- INORG 제품 -->
    <script type="text/javascript" src="./softin/softin.js"></script>           <!-- 라이센스 정보 -->
    <script type="text/javascript" src="./softin/inorg/inorginfo.js"></script>  <!-- 생성시 필요한 함수 및 내부설정용 파일 -->
    <script type="text/javascript" src="./softin/inorg/inorg.js"></script>      <!-- core file -->

    <!-- IBSheet 제품 -->

    <script type="text/javascript" src="./softin/ibsheet/ibsheet.js"></script>
    <script type="text/javascript" src="./softin/ibsheet/ibsheetinfo.js"></script>

    <!-- 화면 소스 -->
    <script type="text/javascript" src="./js/popup.js"></script>
    <script type="text/javascript" src="./js/org_view.js"></script>
    <script type="text/javascript" src="./js/tree_view.js"></script>
    <script type="text/javascript" src="./js/app.js"></script>

</head>

<body>
    <!-- Org -->
    <div class="row" style="height:100%; margin: 0 !important;">
        <div class="col-md-3" style="height:100%;">
            <div id="treeArea"></div>
        </div>
        <div class="col-md-9" style="height:100%;">

            <div id="button_view" class="container-fluid form-inline" style="text-align: right;">

                <select id="templateSelect" class="form-control">
                    <option>템플릿 변경</option>
                    <option>OrgBox</option>
                    <option>PhotoBox</option>
                    <option>OrgType1Box</option>
                    <option>OrgType2Box</option>
                    <option>OrgType3Box</option>
                    <option>OrgType4Box</option>
                    <option>리스트템플릿</option>
                </select>

                <input id="largeView" type="button" class="btn btn-light" value="확대"></input>

                <input id="smallView" type="button" class="btn btn-light" value="축소"></input>

                <input id="basicView" type="button" class="btn btn-light" value="기본크기"></input>

                <input id="autoView" type="button" class="btn btn-light" value="화면에 맞추기"></input>

                <button id="nodeLevelUp" type="button" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-arrow-up" aria-hidden="true"></span>
                </button>
                <input id="autoView" type="button" class="btn btn-default" disabled="disabled" value="개별 레벨 제어"></input>
                <button id="nodeLevelDown" type="button" class="btn btn-default btn-sm">
                    <span class="glyphicon glyphicon-arrow-down" aria-hidden="true"></span>
                </button>
                <input type="hidden" id="nodeLevel">

                <input id="savePop" type="button" class="btn btn-info" value="저장"></input>

                <input id="levelSort" type="button" class="btn btn-primary" value="레벨정렬 ON"></input>

                <input id="suppSort" type="button" class="btn btn-primary" value="보조자정렬 ON"></input>

            </div>
            <div id="viewOrg" style="height: 100%;"></div>

        </div>
    </div>

    <!-- modal-->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-muted" id="myModalLabel"><img src="img/search.png" width="20"
                            height="20"> 사원 정보</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-borderless">
                        <tr>
                            <td rowspan="4" width="200px">
                                <img id="pictureNum" width="180px">
                            </td>
                            <td class="col-2">성명</td>
                            <td class="col-10">
                                <input class="form-control" id="orgName">
                                <input type="hidden" id="pKey">
                            </td>
                        </tr>
                        <tr>
                            <td class="col-2">직급</td>
                            <td class="col-10">
                                <input id="orgPosition" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td class="col-2">부서명</td>
                            <td class="col-10">
                                <input id="orgDept" class="form-control">
                            </td>
                        </tr>
                        <tr>
                            <td class="col-2">보조자</td>
                            <td class="col-10">
                                <input id="orgSupp" class="form-control">
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" id="btnSave" data-dismiss="modal"
                        aria-label="Close">저장</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">닫기</button>
                </div>
            </div>
        </div>
    </div>

    <!--save modal-->
    <div class="modal fade" id="saveModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-muted" id="myModalLabel"> 저장 방식 선택</h4>
                </div>
                <div class="modal-body">
                    <div class="container form-inline">
                        <span class="input-group-text">저장할 이름</span>

                        <input id="saveName" type="text" class="form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <input id="saveAsImage" type="button" class="btn btn-primary" value="이미지 저장" data-dismiss="modal"
                        aria-label="Close"></input>
                    <input id="saveAsJson" type="button" class="btn btn-primary" value="Json 저장" data-dismiss="modal"
                        aria-label="Close"></input>
                </div>
            </div>
        </div>
    </div>

</body>


</html>