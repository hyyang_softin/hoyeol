inapp.add( "popup", {
    init: function () {
        var selfThis = this;

        $( "#btnSave" ).on( "click", function () {
            selfThis.save();

        } );
    },
    //데이터 받기
    actionMap: {
        "org_view": {
            "click_node": function ( prop ) {
                var data;
                data = prop.data;

                if ( data.binding == "emp_nm" ) {
                    this.show( data );
                }

            },
            "select_save_type": function () {
                this.selectSaveTypeShow();
            }
        },
        "tree_view": {
            "dbl_click": function ( prop ) {
                this.show( prop.data );
            }
        }
    },
    // 일반적인 데이터 표시 팝업을 보여준다.
    show: function ( nodeJsonData ) {
        $( '#pKey' ).val( nodeJsonData.fields.pkey.value ); //고유값
        $( '#pictureNum' ).attr( "class", "img-responsive" ); //이미지 반응형(bootstrap)
        $( '#pictureNum' ).attr( "src", nodeJsonData.fields.photo.value ); //이미지

        $( '#orgName' ).val( nodeJsonData.fields.emp_nm.value ); //이름
        $( '#orgPosition' ).val( nodeJsonData.fields.position.value ); //직급
        $( '#orgDept' ).val( nodeJsonData.fields.dept_nm.value ); //부서명
        $( '#orgSupp' ).val( nodeJsonData.supporter ); //서포터 여부
        $( '#nodeLevel' ).val( nodeJsonData.level ); //레벨값
        $( '#modal' ).modal( 'show' );
    },
    // 팝업 내에 데이터 변경후 저장 버튼을 누를때
    save: function () {
        var saveJson = {};
        //데이터를 받아 JSON으로
        saveJson.pKey = $( '#pKey' ).val();
        saveJson.emp_nm = $( '#orgName' ).val();
        saveJson.position = $( '#orgPosition' ).val();
        saveJson.dept_nm = $( '#orgDept' ).val();
        saveJson.supporter = $( '#orgSupp' ).val();
        saveJson.level = $( '#nodeLevel' ).val();
        //데이터 넘김
        inapp.raise( "popup", {
            action: "change_data",
            data: saveJson
        } );

    },
    // 저장 버튼을 누를때 선택되는 팝업을 보여준다
    selectSaveTypeShow: function () {
        var selfThis = this;

        $( '#saveModal' ).modal( 'show' );

        $( "#saveAsImage" ).off().on( "click", function () {
            var fileName = $( '#saveName' ).val();

            selfThis.saveFile( 1, fileName );
            fileName = "";

        } );


        $( "#saveAsJson" ).off().on( "click", function () {
            var fileName = $( '#saveName' ).val();

            selfThis.saveFile( 2, fileName );
            fileName = "";

        } );

    },
    //저장 파일 생성 함수
    saveFile: function ( fileType, fileName ) {
        if ( fileType == 1 ) {
            if ( fileName != "" ) {
                viewOrg.saveAsImage( {
                    filename: fileName
                } );
            } else {
                viewOrg.saveAsImage();
            }
        } else if ( fileType == 2 ) {
            if ( fileName != "" )
                viewOrg.saveAsJson( {
                    filename: fileName
                } );
            else
                viewOrg.saveAsJson();
        }
        $( '#saveName' ).val( "" );
    }
} );