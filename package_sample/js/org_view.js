//var org_view = {
inapp.add( "org_view", {
    init: function () {
        this.resize();
        this.create();
        this.button();
    },
    actionMap: {
        "tree_view": {
            "click_cell": function ( tree_data ) {
                this.selectNode( tree_data.data.dept_cd, true );
            },
            "click_tree_expand": function ( tree_data ) {
                this.expandNode( tree_data.data.key, tree_data.data.expand );
            }
        },
        "popup": {
            "change_data": function ( savedata ) {

                this.save( savedata.data );
            }
        }
    },
    resize: function () {
        var orgView, window_h, button_view_h;

        orgView = this;

        window_h = $( window ).height(); //지금 윈도우의 높이를 구한다.

        button_view_h = $( "#button_view" ).height(); // 버튼의 높이를 구한다.

        $( "#viewOrg" ).height( window_h - button_view_h ); //둘을 뺀다.

        $( window ).resize( function () {
            window_h = $( window ).height();

            button_view_h = $( "#button_view" ).height();
            //리사이징 작업
            $( "#viewOrg" ).height( window_h - button_view_h );

            orgView.scale( "fullfit" );
        } );
    },
    create: function () {
        var option, self;

        self = this;

        option = {
            layout: {
                level: true, // 레벨 정렬 사용여부
                supporter: true // 보조자 사용여부
            },
            defaultTemplate: {
                link: { // 연결선 스타일
                    style: {
                        borderWidth: 2,
                        borderColor: "#CCC",
                        borderStyle: "solid"
                    }
                }
            },
            event: {
                onClick: function ( evt ) {
                    var node, jsonData;
                    //노드가 아닐경우 실행안함
                    if ( !evt.key ) {
                        return
                    }

                    if ( evt.isMember ) {
                        node = evt.member;
                    } else {
                        node = evt.node;
                    }
                    //라벨일 경우 실행안함
                    if ( node.isLabel() ) {
                        return;
                    }

                    jsonData = node.fields().toJSON();
                    //데이터를 전부 전달.
                    inapp.raise( self.name, {
                        action: "click_node",
                        data: {
                            binding: evt.binding,
                            fields: jsonData,
                            supporter: node.supporter(),
                            level: node.level()
                        }
                    } );
                },
                onExpandButtonClick: function ( evt ) {
                    //확장 버튼 클릭시 발생되는 이벤트.
                    //키값과 열/닫힘 상태 전달
                    inapp.raise( self.name, {
                        action: "click_node_expand",
                        key: evt.key,
                        isExpand: evt.isExpand
                    } );
                },
                onDiagramLoadEnd: function ( evt ) {
                    //로드가 끝나면 발생되는 이벤트.
                    var jsonData, jsonArray;
                    
                    jsonData = {};
                    jsonArray = [];
                    // 조직도 데이터를 트리 데이터 형식으로 가공
                    evt.org.nodes( "" ).foreach( function () {
                        var sData = {};

                        if ( !this.isLabel() && !this.isMember() ) {

                            this.fields().foreach( function () {
                                if ( this.name() == "pkey" ) {
                                    sData.dept_cd = this.value();
                                } else {
                                    sData[ this.name() ] = this.value();
                                }
                            } );
                            sData.level = this.level() - 1;
                            sData.supporter = this.supporter();
                            jsonArray.push( sData );
                        }

                    } );

                    jsonData.data = jsonArray;
                    //데이터 전달
                    inapp.raise( self.name, {
                        action: "org_data",
                        data: jsonData

                    } );
                }
            }
        }

        createINOrg( "viewOrg", option );

        this.load();
    },
    load: function () {
        viewOrg.loadJson( {
            url: "./data/sample.json"
        } );
    },
    save: function ( saveJson ) {
        var node = viewOrg.nodes( saveJson.pKey );

        node.fields( "emp_nm" ).value( saveJson.emp_nm );
        node.fields( "position" ).value( saveJson.position );
        node.fields( "dept_nm" ).value( saveJson.dept_nm );

        if ( saveJson.supporter != "" ) {
            node.supporter( parseInt( saveJson.supporter ) );
        } else {
            node.supporter( null );
        }
    },
    button: function () {
        var orgView = this;

        /// 아래와 같이 템플릿 변경시 click 을 사용하면 select 를 누를때와 템플릿명을 선택할때 2번 발생하게 되요. change 로 변경 후 사용하세요^^
        $( "#templateSelect" ).on( "change", function () {
            var templateSelect;
            templateSelect = $( this ).val();

            orgView.template( templateSelect );
        } );

        $( "#largeView" ).on( "click", function () {
            orgView.zoom( "in" );
        } );

        $( "#smallView" ).on( "click", function () {
            orgView.zoom( "out" );
        } );

        $( "#basicView" ).on( "click", function () {
            orgView.scale( "fit" );

        } );

        $( "#autoView" ).on( "click", function () {
            orgView.scale( "fullfit" );
        } );

        $( "#nodeLevelUp" ).on( "click", function () {
            orgView.nodeLevel( 0 );
        } );

        $( "#nodeLevelDown" ).on( "click", function () {
            orgView.nodeLevel( 1 );
        } );

        $( "#levelSort" ).on( "click", function () {
            var sort = $( "#levelSort" ).val();
            if ( sort == "레벨정렬 ON" ) {
                orgView.allLevel( false );
            } else {
                orgView.allLevel( true );
            }

        } );

        $( "#suppSort" ).on( "click", function () {
            var sort = $( "#suppSort" ).val();
            if ( sort == "보조자정렬 ON" ) {
                orgView.allSupp( false );
            } else {
                orgView.allSupp( true );
            }
        } );

        $( "#savePop" ).on( "click", function () {

            inapp.raise( "org_view", {
                action: "select_save_type"
            } );
        } );

    },
    template: function ( params ) {
        viewOrg.nodes( "" ).foreach( function () {

            if ( this.template() != "Label" ) {
                this.template( params );
            }
        } );
    },
    zoom: function ( params ) {
        if ( params == "in" ) {
            viewOrg.zoomIn();
        } else {
            viewOrg.zoomOut();
        }
    },
    scale: function ( params ) {
        viewOrg.scale( params );
    },
    nodeLevel: function ( params ) {
        var node;
        node = viewOrg.nodes.selected(); /// 반복해서 쓰이는 것은 하나의 변수에 담아서 처리하는게 가독성 측면에서 좋아요!

        if ( node != null ) {
            if ( params == 1 ) {
                node.level( node.level() + 1 );
            } else {
                node.level( node.level() - 1 );
            }
        }
    },
    allLevel: function ( params ) {
        if ( params != true ) {
            viewOrg.layout.level( params );
            $( "#levelSort" ).attr( "class", "btn btn btn-outline-primary" );
            $( "#levelSort" ).val( "레벨정렬 OFF" );
        } else {
            viewOrg.layout.level( params );
            $( "#levelSort" ).attr( "class", "btn btn btn-primary" );
            $( "#levelSort" ).val( "레벨정렬 ON" );
        }
    },
    allSupp: function ( params ) {
        if ( params != true ) {
            viewOrg.layout.supporter( params );
            $( "#suppSort" ).attr( "class", "btn btn btn-outline-primary" );
            $( "#suppSort" ).val( "보조자정렬 OFF" );
        } else {
            viewOrg.layout.supporter( params );
            $( "#suppSort" ).attr( "class", "btn btn btn-primary" );
            $( "#suppSort" ).val( "보조자정렬 ON" );
        }
    },
    expandNode: function ( pKey, value ) {
        //노드 펼치기 기능.
        // 파라미터는 pKey는 노드 pKey값, value 는 포커싱여부 true false
        viewOrg.nodes( pKey ).expand( value );
    },
    selectNode: function ( pKey, value ) {
        //노드 포커싱 기능. 클릭 효과.
        // 파라미터는 pKey는 노드 pKey값, value 는 포커싱여부 true false
        viewOrg.nodes( pKey ).select( value );
    }
} );