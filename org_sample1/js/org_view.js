function initINOrg() {
    var option;

    option = {
        layout: {
            level: true, // 레벨 정렬 사용여부
            supporter: true // 보조자 사용여부
        },
        defaultTemplate: {
            link: { // 연결선 스타일
                style: {
                    borderWidth: 2,
                    borderColor: "#CCC",
                    borderStyle: "solid"
                }
            }
        },
        event: {
            onClick: function ( evt ) {
                var node;
                var nodeJsonData = {};

                if ( !evt.key ) {
                    return;
                };

                if ( evt.binding != "emp_nm" ) {
                    return;
                };

                if ( evt.isMember == true ) {
                    node = evt.member;
                } else {
                    node = evt.node;
                };

                node.fields().foreach( function () {
                    nodeJsonData[ this.name() ] = this.value();
                } );

                $( '#pictureNum' ).attr( "src", nodeJsonData.photo ); //이미지
                $( '#pictureNum' ).attr( "class", "img-responsive" ); //이미지 반응형(bootstrap)
                $( '#orgName' ).html( nodeJsonData.emp_nm ); //이름
                $( '#orgPosition' ).html( nodeJsonData.position ); //직급
                $( '#orgDept' ).html( nodeJsonData.dept_nm ); //부서명
                $( '#modal' ).modal( 'show' );

            }
        }
    }

    createINOrg( "viewOrg", option );

    load();
}

function load() {
    viewOrg.loadJson( {
        url: "./data/sample.json"
    } );
}

$( document ).ready( function () {
    console.log( "시작" );
    initINOrg();
} );