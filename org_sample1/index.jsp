<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="ko">

<head>
    <title>INORG Sample</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="./lib/bootstrap/css/bootstrap.min.css">

    <style>
        html,
        body {
            height: 100% !important;
        }
    </style>

    <!-- 외부 라이브러리 -->
    <script type="text/javascript" src="./lib/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/bootstrap/js/bootstrap.min.js"></script>

    <!-- INORG 제품 3개는 필수! -->
    <script type="text/javascript" src="./softin/softin.js"></script> <!-- 라이센스 정보 -->
    <script type="text/javascript" src="./softin/inorg/inorginfo.js"></script> <!-- 생성시 필요한 함수 및 내부설정용 파일 -->
    <script type="text/javascript" src="./softin/inorg/inorg.js"></script> <!-- core file -->

    <!-- 화면 소스 -->
    <script type="text/javascript" src="./js/org_view.js"></script>
</head>

<body>
    <!--조직도-->
    <div id="viewOrg" class="container-fluid" style="height: 100%;"></div>
    <!-- modal-->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">×</span></button>
                    <h4 class="modal-title text-muted" id="myModalLabel"><img src="img/search.png" width="20"
                            height="20"> 사원 정보</h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered">
                        <tr>
                            <td rowspan="3" width="180px">
                                <img id="pictureNum">
                            </td>
                            <td class="col-2">성명</td>
                            <td class="col-10">
                                <div id="orgName" class="form-control">성명</div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-2">직급</td>
                            <td class="col-10">
                                <div id="orgPosition" class="form-control" >
                                    직 급 입 력
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="col-2">부서명</td>
                            <td class="col-10">
                                <div id="orgDept" class="form-control">부서명</div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal" aria-label="Close">닫기</button>
                </div>
            </div>
        </div>
    </div>
</body>

</html>