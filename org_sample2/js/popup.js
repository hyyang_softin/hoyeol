var popup = {
    init: function () {

        // JQuery 버튼 클릭 메서드는 한번만 호출하면 된다.
        $( "#btnSave" ).on( "click", function () {
            // save 함수 호출

            popup.save();
        } );
    },
    /**
     * 팝업을 보여준다.
     */
    show: function ( nodeJsonData ) {
        $( '#pictureNum' ).attr( "src", nodeJsonData.photo ); //이미지
        $( '#pictureNum' ).attr( "class", "img-responsive" ); //이미지 반응형(bootstrap)
        $( '#orgName' ).val( nodeJsonData.emp_nm ); //이름
        $( '#orgPosition' ).val( nodeJsonData.position ); //직급
        $( '#orgDept' ).val( nodeJsonData.dept_nm ); //부서명
        $( '#orgSupp' ).val( nodeJsonData.supporter ); //서포터 여부
        $( '#pKey' ).val( nodeJsonData.pkey ); //고유값
        $( '#nodeLevel' ).val( nodeJsonData.level ); //레벨값

        $( '#modal' ).modal( 'show' );
    },
    /**
     * 팝업 내에 저장 버튼을 누를때
     */
    save: function () {

        var saveJson = {};

        saveJson.pKey = $( '#pKey' ).val();
        saveJson.emp_nm = $( '#orgName' ).val();
        saveJson.position = $( '#orgPosition' ).val();
        saveJson.dept_nm = $( '#orgDept' ).val();
        saveJson.supporter = $( '#orgSupp' ).val();
        saveJson.level = $( '#nodeLevel' ).val();
        org_view.save( saveJson );

    },
    selectSaveType: function () {

        $( '#saveModal' ).modal( 'show' );

        $( "#saveAsImage" ).on( "click", function () {
            var fileName = $( '#saveName' ).val();

            popup.saveFile( 1, fileName );
            fileName= "";
        } );

        $( "#saveAsJson" ).on( "click", function () {
            var fileName = $( '#saveName' ).val();

            popup.saveFile( 2, fileName );
            fileName= "";
        } );
    },
    saveFile: function ( fileType, fileName ) {
        if ( fileType == 1 ) {
            if ( fileName != "" ) {
                viewOrg.saveAsImage( {
                    filename: fileName
                } );
            } else {
                viewOrg.saveAsImage();
            }
        } else if ( fileType == 2 ) {
            if ( fileName != "" )
                viewOrg.saveAsJson( {
                    filename: fileName
                } );
            else
                viewOrg.saveAsJson();
        }
        $( '#saveName' ).val("");
    }
};

$( document ).ready( function () {
    popup.init();
} );