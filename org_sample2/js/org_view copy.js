var org_view = {
    init: function () {

        this.create();
        this.button();

    },
    create: function () {
        var option;

        option = {
            layout: {
                level: true, // 레벨 정렬 사용여부
                supporter: true // 보조자 사용여부
            },
            defaultTemplate: {
                link: { // 연결선 스타일
                    style: {
                        borderWidth: 2,
                        borderColor: "#CCC",
                        borderStyle: "solid"
                    }
                }
            },
            event: {
                onClick: function ( evt ) {
                    var node, nodeJsonData;

                    nodeJsonData = {};

                    if ( !evt.key ) {
                        return;
                    };


                    if ( evt.binding != "emp_nm" ) {
                        return;
                    };

                    if ( evt.isMember == true ) {
                        node = evt.member;
                    } else {
                        node = evt.node;
                    };

                    node.fields().foreach( function () {
                        nodeJsonData[ this.name() ] = this.value();
                    } );

                    if ( node.isSupporter() == true ) {
                        nodeJsonData[ "supporter" ] = node.supporter();
                    } else {
                        nodeJsonData[ "supporter" ] = null;
                    }
                    nodeJsonData.level = node.level();



                    popup.show( nodeJsonData );

                },
                onExpandButtonClick: function ( evt ) {
                    var sampleData;

                    if ( evt.expandable == true ) {
                        sampleData = {
                            "orgData": [ {
                                "template": "PhotoBox",
                                "layout": {
                                    "level": 3
                                },
                                "fields": {
                                    "pkey": "5555",
                                    "rkey": "3000",
                                    "dept_nm": {
                                        "value": "테스트",
                                        "style": {
                                            "backgroundColor": "#D46231"
                                        }
                                    },
                                    "position": "사원",
                                    "emp_nm": "테스트"
                                }
                            } ]
                        }

                        evt.org.loadJson( {
                            data: sampleData,
                            append: true
                        } );
                    }

                }
            }
        }

        createINOrg( "viewOrg", option );

        this.load();
    },
    save: function ( saveJson ) {

        var node = viewOrg.nodes( saveJson.pKey );

        node.fields( "emp_nm" ).value( saveJson.emp_nm );
        node.fields( "position" ).value( saveJson.position );
        node.fields( "dept_nm" ).value( saveJson.dept_nm );

        if ( saveJson.supporter != "" ) {
            node.supporter( parseInt( saveJson.supporter ) );
        } else {
            node.supporter( null );
        }
    },
    button: function () {

        $( "#templateSelect" ).on( "click", function () {
            var templateSelect = $( "#templateSelect option:selected" ).val();
            org_view.template( templateSelect );
        } );

        $( "#largeView" ).on( "click", function () {
            org_view.zoom( 1 );
        } );

        $( "#smallView" ).on( "click", function () {
            org_view.zoom( 0 );
        } );

        $( "#basicView" ).on( "click", function () {
            org_view.scale( "fit" );

        } );

        $( "#autoView" ).on( "click", function () {
            org_view.scale( "fullfit" );
        } );

        $( "#nodeLevelUp" ).on( "click", function () {
            org_view.nodeLevel( 0 );
        } );

        $( "#nodeLevelDown" ).on( "click", function () {
            org_view.nodeLevel( 1 );
        } );

        $( "#levelSort" ).on( "click", function () {
            var sort = $( "#levelSort" ).val();
            if ( sort == "레벨정렬 ON" ) {
                org_view.allLevel( false );
            } else {
                org_view.allLevel( true );
            }

        } );

        $( "#suppSort" ).on( "click", function () {
            var sort = $( "#suppSort" ).val();
            if ( sort == "보조자정렬 ON" ) {
                org_view.allSupp( false );
            } else {
                org_view.allSupp( true );
            }
        } );

        $( "#savePop" ).on( "click", function () {
            popup.selectSaveType();
        } );

    },

    template: function ( params ) {
        viewOrg.nodes( "" ).foreach( function () {

            if ( this.template() != "Label" ) {
                this.template( params );
            }
        } );
    },
    zoom: function ( params ) {
        if ( params == 1 ) {
            viewOrg.zoomIn();
        } else {
            viewOrg.zoomOut();
        }
    },
    scale: function ( params ) {
        viewOrg.scale( params );
    },
    nodeLevel: function ( params ) {
        if ( viewOrg.nodes.selected() != null ) {
            if ( params == 1 ) {
                viewOrg.nodes.selected().level( parseInt( viewOrg.nodes.selected().level() ) + 1 );
            } else {
                viewOrg.nodes.selected().level( parseInt( viewOrg.nodes.selected().level() ) - 1 );
            }
        }
    },
    allLevel: function ( params ) {
        if ( params != true ) {
            viewOrg.layout.level( params );
            $( "#levelSort" ).attr( "class", "btn btn btn-outline-primary" );
            $( "#levelSort" ).val( "레벨정렬 OFF" );
        } else {
            viewOrg.layout.level( params );
            $( "#levelSort" ).attr( "class", "btn btn btn-primary" );
            $( "#levelSort" ).val( "레벨정렬 ON" );
        }
    },
    allSupp: function ( params ) {
        if ( params != true ) {
            viewOrg.layout.supporter( params );
            $( "#suppSort" ).attr( "class", "btn btn btn-outline-primary" );
            $( "#suppSort" ).val( "보조자정렬 OFF" );
        } else {
            viewOrg.layout.supporter( params );
            $( "#suppSort" ).attr( "class", "btn btn btn-primary" );
            $( "#suppSort" ).val( "보조자정렬 ON" );
        }
    },
    load: function () {
        viewOrg.loadJson( {
            url: "./data/sample.json"
        } );
    }
}

$( document ).ready( function () {
    console.log( "시작" );
    org_view.init();
} );