//var org_view = {
inapp.add( "org_view", {
    init: function () {
        this.create();
    },
    actionMap: {
        "tree_view": {
            "click_cell": function ( tree_data ) {
                this.selectNode( tree_data.data.dept_cd, true );
            },
            "click_tree_expand": function ( tree_data ) {

                if ( tree_data.data.expand === 0 ) {
                    this.expandNode( tree_data.data.dept_cd, true );
                } else {
                    this.expandNode( tree_data.data.dept_cd, false );
                }

            }
        }
    },
    create: function () {
        var option;

        option = {
            layout: {
                level: true, // 레벨 정렬 사용여부
                supporter: true // 보조자 사용여부
            },
            defaultTemplate: {
                link: { // 연결선 스타일
                    style: {
                        borderWidth: 2,
                        borderColor: "#CCC",
                        borderStyle: "solid"
                    }
                }
            },
            event: {
                onClick: function ( evt ) {
                    var node ;
                    if ( !evt.key ) {
                        return
                    }

                    if ( evt.node.isLabel() ) {
                        return;
                    }

                    if ( evt.isMember ) {
                        node = evt.member;
                    }else{
                        node = evt.node;
                    }

                    inapp.raise( "org_view", {
                        action: "click_node",
                        key: node.fields().value()
                    } );

                },
                onExpandButtonClick: function ( evt ) {

                    inapp.raise( "org_view", {
                        action: "click_node_expand",
                        key: evt.key,
                        isExpand: evt.isExpand
                    } );

                },
                onDiagramLoadEnd: function ( evt ) {
                    //로드가 끝나면 발생되는 이벤트.
                    var jsonData, jsonArray;
                    // 조직도 데이터를 트리 데이터 형식으로 가공
                    jsonData = {};
                    jsonArray = new Array();

                    evt.org.nodes( "" ).foreach( function () {
                        var sData = {};

                        if ( !this.isLabel() && !this.isMember() ) {

                            this.fields().foreach( function () {
                                if ( this.name() == "pkey" ) {
                                    sData.dept_cd = this.value();
                                } else {
                                    sData[ this.name() ] = this.value();
                                }
                            } );
                            sData.Level = this.level() - 1;
                            jsonArray.push( sData );
                        }


                    } );

                    jsonData.data = jsonArray;

                    inapp.raise( "org_view", {
                        action: "org_data",
                        data: jsonData

                    } );
                }
            }
        }

        createINOrg( "viewOrg", option );

        this.load();
    },
    load: function () {
        viewOrg.loadJson( {
            url: "./data/sample.json"
        } );
    },
    expandNode: function ( pKey, value ) {
        //노드 펼치기 기능.
        // 파라미터는 pKey는 노드 pKey값, value 는 포커싱여부 true false
        viewOrg.nodes( pKey ).expand( value );
    },
    selectNode: function ( pKey, value ) {
        //노드 포커싱 기능. 클릭 효과.
        // 파라미터는 pKey는 노드 pKey값, value 는 포커싱여부 true false
        viewOrg.nodes( pKey ).select( value );
    }
} );