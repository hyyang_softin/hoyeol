//var tree_view = {
inapp.add( "tree_view", {
    cfg: {

    },
    actionMap: {
        "org_view": {
            "org_data": function ( org_data ) {
                this.load( org_data.data );
            },
            "click_node": function ( prop ) {
                this.setSelectRow( prop.key );
            },
            "click_node_expand": function ( expand_data ) {
                this.setRowExpanded( expand_data.key, expand_data.isExpand );
            }
        }
    },
    init: function () {
        this.create();
    },
    createSheet: function () {
        /**
         * IBSheet를 생성
         * @param 	{object}				IBSheet가 생성될 대상 컨테이너
         * @param	{object}				IBSheet의 ID
         * @param	{String}	"100%"		너비
         * @param	{String}	"100%"		높이
         */
        createIBSheet2( document.getElementById( "treeArea" ), "treeSheet", "100%", "100%" );
    },
    create: function () {
        // 개발자 가이드 - 5.1 항목을 보고 작성
        var option = {};

        this.createSheet();

        option = {
            Cfg: {
                SearchMode: smLazyLoad, // smLazyLoad : 스크롤 조회
                Page: 30, //한번에 가져올 데이터 갯수
                AutoFitColWidth: "search|resize|init" //기본값 "" ,조회 및 로드 시점, 시트 Resize시점, 초키화 및 Removeall 호출 시점
            },
            HeaderMode: {
                Sort: 0, //헤더 클릭을 통한 컬럼 Sort 허용 여부.0 사용안함, 1 sort기능 사용(디폴트), 2 아이콘만 표시, 헤더셀만 사용
                ColMove: 0, //헤더칼럼 이동여부
                ColResize: 1, //컬럼 너비 ReSize 여부
                HeaderCheck: 0 //헤더에 전체 체크 표시 여부
            },
            Cols: [ { //칼럼에 대한 정보. 시트에서 표현할 정보 나열
                    Header: "조직코드", //제목
                    Type: "Text", //타입
                    Width: 50, // 넓이
                    SaveName: "dept_cd", //INOrg 필드에 있는거 중에 가져올 데이터 이름.
                    Align: "Center", //정렬
                    Edit: 0, // 편집 가능여부. 0-불가 1- 가능
                    Hidden: 0 // 숨김여부 0-보임 1-숨김
                },
                {
                    Header: "조직명",
                    Type: "Text",
                    Width: 100,
                    SaveName: "dept_nm",
                    Align: "Left",
                    Edit: 0,
                    TreeCol: 1
                },
                {
                    Header: "성명",
                    Type: "Text",
                    Width: 50,
                    SaveName: "emp_nm",
                    Align: "Center",
                    Edit: 0,
                    Hidden: 0
                }
            ]
        }

        IBS_InitSheet( treeSheet, option ); // treeSheet는 IBSheet의 createIBSheet2에서 생성된 전역객체

        this.bind();

        //this.loadSample();
    },
    bind: function () {
        var treeView;

        treeView = this;
        //tree_view를 직접 불러올수 없다.

        function addEvents( treeSheet, callbacks ) {
            //IBSheet 기능을 강제로 연결하여 사용할수 있도록 함.
            for ( var prop in callbacks ) {
                window[ treeSheet + "_" + prop ] = callbacks[ prop ];
            }
        }

        addEvents( treeSheet.id, {
            OnSelectCell: function ( OldRow, OldCol, NewRow, NewCol, isDelete ) {
                //셀 클릭시.
                var rowJson;

                //rowJson = tree_view.getRowJson( NewRow );
                ////직접 불러올수 없음

                rowJson = treeView.getRowJson( NewRow );
                // this를 참조하여 자기자신을 바라보게끔 해서 가져온다.

                if ( rowJson === -1 ) {
                    return;
                }

                inapp.raise( "tree_view", {
                    action: "click_cell",
                    data: rowJson
                } );

            },
            OnBeforeExpand: function ( row, expand ) {
                // 셀 접기 기능.
                var treeSelectJson;

                //rowJson = tree_view.getRowJson( NewRow );
                ////직접 불러올수 없음

                treeSelectJson = treeView.getRowJson( row );
                // this를 참조하여 자기자신을 바라보게끔 해서 가져온다.

                if ( treeSelectJson === -1 ) {
                    return;
                }

                inapp.raise( "tree_view", {
                    action: "click_tree_expand",
                    data: {
                        expand: expand,
                        data: treeSelectJson
                    }
                } );

            }
        } );
    },
    load: function ( data ) {
        treeSheet.LoadSearchData( data );
    },
    loadSample: function () {
        var jsonData;

        jsonData = {
            "data": [ {
                    "dept_cd": "1",
                    "dept_nm": "개발본부",
                    "emp_nm": "홍길동",
                    "Level": 0
                },
                {
                    "dept_cd": "2",
                    "dept_nm": "개발실",
                    "emp_nm": "강감찬",
                    "Level": 1
                },
                {
                    "dept_cd": "3",
                    "dept_nm": "개발1팀",
                    "emp_nm": "연개소문",
                    "Level": 2
                },
                {
                    "dept_cd": "4",
                    "dept_nm": "개발1팀",
                    "emp_nm": "홍길동",
                    "Level": 2
                }
            ]
        };

        treeSheet.LoadSearchData( jsonData );
    },
    setRowExpanded: function ( row, check ) {
        //펼침여부 설정 함수
        // 파라미터  row 펼칠 행의 pKey , check 1 열림 0 닫힘
        var findRow;
        findRow = treeSheet.FindText( "dept_cd", row );

        if ( findRow === -1 ) {
            return;
        }

        treeSheet.SetRowExpanded( findRow, check );
    },
    setSelectRow: function ( params ) {
        // 시트 강제 포커싱 기능. 클릭한 효과.
        // 파라미터는 찾을 row값
        var findRow;

        findRow = treeSheet.FindText( "dept_cd", params );

        if ( findRow === -1 ) {
            return;
        }

        treeSheet.SetSelectRow( findRow );

    },
    getRowJson: function ( params ) {
        //컬럼 데이터 행의 json 값 리턴 함수
        //파라미터는 찾을 row 번호.
        var value;

        value = treeSheet.GetRowJson( params );

        return value;
    }
} );