inapp.add( "echart_data_manger", {
    init: function () {

        this.create();

    },
    create: function () {
        var self;

        self = this;

        $.get( './data/data.json' ).done( function ( data ) {

            var userData,yearData, trueData, i, length; //총데이터 , 길이등

            var continuousYear, oldSalaryArg, newSalaryArg, incentiveSum, evaluationText, genderSumM, genderSumW, newSalaryM, newSalaryW, ageM, ageW ;// 변수들


            userData = data.user_data;
            yearData = data.year_data;
            trueData = {};

            continuousYear = 0;
            oldSalaryArg = 0;
            newSalaryArg = 0;
            incentiveSum = 0;
            evaluationText = "";
            genderSumM = 0;
            genderSumW = 0;
            newSalaryM = 0;
            newSalaryW = 0;
            ageM = 0;
            ageW = 0;
            dutyText = "";
            occupationsText = "";


            length=userData.length;

            for (  i = 0; i < length; i++ ) {
                continuousYear = continuousYear + userData[ i ].continuous_year;
                oldSalaryArg = oldSalaryArg + userData[ i ].oldSalary;
                newSalaryArg = newSalaryArg + userData[ i ].newSalary;

                evaluationText += userData[ i ].evaluation;
                dutyText += userData[ i ].duty + ",";
                occupationsText += userData[ i ].occupations + ",";
                if ( userData[ i ].incentive ) {
                    incentiveSum++;
                }
                if ( userData[ i ].gender == "M" ) {
                    genderSumM++;
                    newSalaryM = newSalaryM + userData[ i ].newSalary;
                    ageM = ageM + userData[ i ].age;
                } else {
                    genderSumW++;
                    newSalaryW = newSalaryW + userData[ i ].newSalary;
                    ageW = ageW + userData[ i ].age;
                }
            }

            // evaluationText = ABACA
            // eval =[];    eval =['A','A','A'];
            // eval.push(data);
            // eval.length = 3


            /*
            evaluationData = {};

            for (  i = 0; i < length; i++ ) {
                eval = userData[ i ].evaluation;

                A

                if(!evaluationData[eval]) {
                    evaluationData[eval] = 0;
                } else {
                    evaluationData[eval]++;
                }


                /*
                evaluationData = {
                    "A" :0,
                    "B":0,
                    "C":0
                }

            }
            */

            trueData = {
                symbol : data.symbol,

                personnelInfo:
                {
                    data1 : [ {
                        value: genderSumM / 1,
                        symbol: data.symbol.oneman
                    }],
                    data2 : [{
                        value: genderSumW / 1,
                        symbol: data.symbol.manyman
                    } ],
                    value: [ {value : (genderSumW / genderSumM).toFixed( 1 ),
                        symbol: "none" }]
                },
                continuousYear: {
                    max: 50,
                    markline: 35,
                    data : [ continuousYear / length ]
                },
                salaryIncrease: {
                    max: null,
                    markline : 85,
                    data : [ ( newSalaryArg - oldSalaryArg ) / oldSalaryArg * 100 ]
                },
                incentiveArg: {
                    max: null,
                    markline : 72,
                    data : [ incentiveSum / length * 100 ]
                },
                budget:{
                    name: [ '집행 예산', '가용 예산' ],
                    data: [ {
                        name: '집행 예산',
                        value: newSalaryArg
                    }, {
                        name: '가용 예산',
                        value: 100000 - newSalaryArg
                    } ],
                    centerdata : [ {
                        name: '총 예산',
                        value: 100000
                    } ]
                },
                evaluation: {
                    name : ['A','B', 'C', 'D', 'E'],
                    data : [ {
                        name: 'A',
                        value: ( evaluationText.match( /A/g ) || [] ).length
                    }, {
                        name: 'B',
                        value: ( evaluationText.match( /B/g ) || [] ).length
                    }, {
                        name: 'C',
                        value: ( evaluationText.match( /C/g ) || [] ).length
                    }, {
                        name: 'D',
                        value: ( evaluationText.match( /D/g ) || [] ).length
                    }, {
                        name: 'E',
                        value: ( evaluationText.match( /E/g ) || [] ).length
                    } ]
                },
                gender: {
                    name : ['남','여'],
                    data : [ {
                        name: '남',
                        value: genderSumM / 1
                    }, {
                        name: '여',
                        value: genderSumW / 1
                    } ]
                },
                laborCosts: {
                    name : ['남', '여'],
                    data : [ {
                        name: '남',
                        value: ( newSalaryM / genderSumM ).toFixed( 0 )
                    }, {
                        name: '여',
                        value: ( newSalaryW / genderSumW ).toFixed( 0 )
                    } ]
                },
                age: {
                    data1 : [ {
                        value: (ageM / genderSumM).toFixed( 1 ),
                        symbol: data.symbol.man
                    }],
                    data2 : [{
                        value: (ageW / genderSumW).toFixed( 1 ),
                        symbol: data.symbol.woman
                    } ],
                    value: []
                },
                type: {
                    name : ['A','B', 'C', 'D', 'E'],
                    data : [ {
                        name: 'A',
                        value: ( evaluationText.match( /A/g ) || [] ).length
                    }, {
                        name: 'B',
                        value: ( evaluationText.match( /B/g ) || [] ).length
                    }, {
                        name: 'C',
                        value: ( evaluationText.match( /C/g ) || [] ).length
                    }, {
                        name: 'D',
                        value: ( evaluationText.match( /D/g ) || [] ).length
                    }, {
                        name: 'E',
                        value: ( evaluationText.match( /E/g ) || [] ).length
                    } ]
                },
                duty: {
                    name : [ '기술', '기획', '디자인', '영업', '스탭' ],
                    data : [ {
                        name: '기술',
                        value: ( dutyText.match( /기술/g ) || [] ).length
                    }, {
                        name: '기획',
                        value: ( dutyText.match( /기획/g ) || [] ).length
                    }, {
                        name: '디자인',
                        value: ( dutyText.match( /디자인/g ) || [] ).length
                    }, {
                        name: '영업',
                        value: ( dutyText.match( /영업/g ) || [] ).length
                    }, {
                        name: '스탭',
                        value: ( dutyText.match( /스탭/g ) || [] ).length
                    } ]
                },
                occupations: {
                    name : [ '경영', '영업', '생산', '전문', '서비스' ],
                    data :  [ {
                        name: '경영',
                        value: ( occupationsText.match( /경영/g ) || [] ).length
                    }, {
                        name: '영업',
                        value: ( occupationsText.match( /영업/g ) || [] ).length
                    }, {
                        name: '생산',
                        value: ( occupationsText.match( /생산/g ) || [] ).length
                    }, {
                        name: '전문',
                        value: ( occupationsText.match( /전문/g ) || [] ).length
                    }, {
                        name: '서비스',
                        value: ( occupationsText.match( /서비스/g ) || [] ).length
                    } ]
                },
                working_hours: {
                    legend: [ '소정', '시간외' ],
                    xAxis : [ {
                        data: [ '1월', '2월', '3월', '4월', '5월', '6월', '7월', '8월', '9월', '10월', '11월', '12월' ]
                    } ],
                    yAxis : [ {
                        name: '평균근로시간(H)',
                        max: 15,
                        min: 0
                    } ],
                    data : [ {
                        name: '소정',
                        type: 'bar',
                        stack: 'one',
                        label: {
                            show: true,
                            formatter: '{c}',
                        },
                        data: []
                    },
                    {
                        name: '시간외',
                        type: 'bar',
                        stack: 'one',
                        label: {
                            show: true,
                            formatter: '{c}',
                        },
                        data: []
                    }]
                },
                vacation_days: [],
                join_number: [],
                leave_number: [],
                time_on_work: [],
                time_off_work: []
            };

            length= yearData.length ;
            for ( i = 0; i < length; i++ ) {

                trueData.working_hours.data[0].data.push( yearData[ i ].working_hours );
                trueData.working_hours.data[1].data.push( yearData[ i ].out_working_hours );
                trueData.vacation_days.push( yearData[ i ].vacation_days );
                trueData.join_number.push( yearData[ i ].join_number );
                trueData.leave_number.push( yearData[ i ].leave_number );
                trueData.time_on_work.push(new Date(yearData[ i ].time_on_work) );
                trueData.time_off_work.push(new Date(yearData[ i ].time_off_work) - new Date(data.year_data[ i ].time_on_work) );
            }

            inapp.raise( self.name, {
                action: "chart_data",
                data: trueData
            } );
        });
    }
});