<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="ko">

<head>
    <title>INORG Sample</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="./lib/bootstrap/css/bootstrap.min.css">

    <style>
        html,
        body {
            height: 100% !important;
        }
        .col-md-6,.col-md-4 {
            text-align: center;
            margin: 0;
        }
    </style>

    <!-- 외부 라이브러리 -->
    <script type="text/javascript" src="./lib/jquery/jquery.min.js"></script>
    <script type="text/javascript" src="./lib/bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./lib/inapp.js"></script>
    <script type="text/javascript" src="./lib/echarts.min.js"></script>

    <!-- INORG 제품 -->
    <!-- <script type="text/javascript" src="./softin/softin.js"></script>
    <script type="text/javascript" src="./softin/inorg/inorginfo.js"></script>
    <script type="text/javascript" src="./softin/inorg/inorg.js"></script>  -->

    <!-- IBSheet 제품 -->
    <!-- <script type="text/javascript" src="./softin/ibsheet/ibsheet.js"></script>
    <script type="text/javascript" src="./softin/ibsheet/ibsheetinfo.js"></script> -->

    <!-- 화면 소스 -->
    <!-- <script type="text/javascript" src="./js/popup.js"></script>
    <script type="text/javascript" src="./js/org_view.js"></script>
    <script type="text/javascript" src="./js/tree_view.js"></script>-->
    <script type="text/javascript" src="./js/app.js"></script>
    <script type="text/javascript" src="./js/echart_view.js"></script>
    <script type="text/javascript" src="./js/echart_data_manger.js"></script>

</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-4">
                <div class="well">
                    <span>근속년수</span>
                    <div class="center-block" id="yearsServiceChart" style="width:350px; height:80px;"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="well">
                    <span>연봉인상률</span>
                    <div class="center-block" id="salaryIncreaseChart" style="width:350px; height:80px;"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="well">
                    <span>인센티브 지급률</span>
                    <div class="center-block" id="incentiveChart" style="width:350px; height:80px;"></div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-4">
                <div class="well">
                    <span>인건비 예산</span>
                    <div class="center-block" id="budgetChart" style="width:350px; height:250px;"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="well">
                    <span>인원 정보</span>
                    <div class="center-block" id="personnelInfoChart" style="width:350px; height:250px;"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="well">
                    <span>평가분포 및 인원</span>
                    <div class="center-block" id="evaluationChart" style="width:350px; height:250px;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="well">
                    <span>성별 비율 및 인원</span>
                    <div class="center-block" id="genderChart" style="width:350px; height:250px;"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="well">
                    <span>평균 연령</span>
                    <div class="center-block" id="ageArgChart" style="width:350px; height:250px;"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="well">
                    <span>성별 비율 및 인건비</span>
                    <div class="center-block" id="laborCostsChart" style="width:350px; height:250px;"></div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-4">
                <div class="well align-middle">
                    <span>직원 유형별 비율 및 인원</span>
                    <div class="center-block" id="typeChart" style="width:350px; height:250px;"></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="well">
                    <span>직군별 비율 및 인원</span>
                    <div class="center-block" id="dutyChart" style="width:350px; height:250px; "></div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="well">
                    <span>직종별 비율 및 인원</span>
                    <div class="center-block" id="occupationsChart" style="width:350px; height:250px;"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="well">
                    <span>월별 평균 근로 시간</span>
                    <div class="center-block" id="workingTimeChart" style="width:600px; height:300px; "></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="well">
                    <span>인당 월별 평균 휴가 사용일수</span>
                    <div class="center-block" id="vacationDaysChart" style="width:600px; height:300px; "></div>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-6">
                <div class="well">
                    <span>월별 평균 출퇴근 시간</span>
                    <div class="center-block" id="timeWorkChart" style="width:600px; height:300px; "></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="well">
                    <span>월별 입/퇴사자 수</span>
                    <div class="center-block" id="joinLeaveChart" style="width:600px; height:300px; "></div>
                </div>
            </div>
        </div>
    </div>
</body>


</html>